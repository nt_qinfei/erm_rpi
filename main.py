#! /usr/bin/env python
# -*- coding: utf-8 -*-
import uart
import mq


if __name__ == "__main__":
    '''
    串口接收与 mqtt 协议互转， 用于机房监控系统
    '''
    uart.serial_init()
    mq.init_mq()

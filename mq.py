# -*- encoding:utf-8 -*-
import threading
import config
import paho.mqtt.client as mqtt


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(config.TOPIC_TO_SUB)


import uart


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" : "+str(msg.payload))
    uart.send_data(str(msg.payload))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message


def send_msg(topic, msg):
    client.publish(topic, payload=msg, qos=0, retain=False)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
def mq_start():
    client.connect(config.MQ_SERVER, config.MQ_PORT, config.MQ_KEEP_ALIVE)
    client.loop_forever()


def init_mq():
    mq_thread = threading.Thread(target=mq_start, args=())
    mq_thread.start()
    # mq_thread.setDaemon(True)


if __name__ == "__main__":
    init_mq()

# -*- encoding:utf-8 -*-
# import machine

# 连接 wifi 信息
# WIFI_SSID="Lexin_Smart"
# WIFI_PWD="lexin123"

# wifi 断开监测时间间隔
# wifi_check_time_s=10
# 芯片 id 的 hash 值
CHPIP_ID = "6636366"

MQ_SERVER = "120.92.84.64"
MQ_PORT = 1883
MQ_KEEP_ALIVE = 10

# MQTT 话题格式
TOPIC_TO_SUB = "er/" + CHPIP_ID + "/#"

UART_PORT = "/dev/ttyUSB0"
# 串口波特率
UART_BAUD_RATE = 115200
